package com.example.demo.controller;


import com.example.demo.model.Card;
import com.example.demo.service.ProcessingService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;

import java.time.Duration;

@RestController
public class ProcessingController {


    final private ProcessingService processingService;

    public ProcessingController(ProcessingService processingService) {
        this.processingService = processingService;
    }

    /**
     * Получение карт с задержкой
     * @param delay задержка в секундах
     * @return поток с данными
     */
    @GetMapping(value = "/all", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    Flux<Card> getAllCard(@RequestParam(defaultValue = "5") Integer delay) {
        var cards = processingService.getAllCards();
        var demonstationDelay = Flux.interval(Duration.ofSeconds(delay));
        return Flux.zip(cards, demonstationDelay).map(Tuple2::getT1);
    }
}
