package com.example.demo.controller;

import com.example.demo.model.Card;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@RestController
public class BusinessController {

    final private WebClient webClient;

    public BusinessController(WebClient webClient) {
        this.webClient = webClient;
    }

    /**
     * Получение первой подходящей карты из потока
     * @param timeout сколько ждать
     * @param debt    порог долга сколько нужно платить
     * @return результат с затраченным временем и ошибкой
     */
    @GetMapping(value = "/getBestCard")
    public Map<String, Object> getBestCard(@RequestParam(defaultValue = "15") Integer timeout,
                                           @RequestParam(defaultValue = "900") Integer debt) {
        var startTime = System.currentTimeMillis();
        AtomicReference<String> error = new AtomicReference<>();
        var response = this.webClient.get().uri("/all")
                .retrieve()
                .onStatus(HttpStatus::isError, resp -> Mono.error(new Exception(resp.statusCode().toString())))
                .bodyToFlux(Card.class)
                .filter(el -> debt < el.getBalance())
                .timeout(Duration.ofSeconds(timeout))
                .doOnError(ex -> error.set(ex.getLocalizedMessage()))
                .onErrorReturn(new Card())
                .blockFirst();
        if (error.get() == null && response == null) {
            error.set("Нет подходящих карт");
        }
        var endTime = System.currentTimeMillis();
        return new HashMap<>() {{
            put("reponse", response);
            put("duration", (endTime - startTime) / 1000);
            put("error", error.get());
        }};
    }
}
