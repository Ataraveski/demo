package com.example.demo.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfiguration {

    @Value("${target.host}")
    private String url = "";

    @Bean
    public WebClient getClient() {
        return WebClient.builder().baseUrl(url).build();
    }

}
