package com.example.demo.service;

import com.example.demo.model.Card;

import java.util.Optional;

public interface BusinessService {
    boolean checkCardBalance(Integer balance);

    Optional<Card> getBestCard();
}
