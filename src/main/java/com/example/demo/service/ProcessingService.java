package com.example.demo.service;

import com.example.demo.model.Card;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProcessingService {
    Flux<Card> getAllCards();

    Mono<Card> getById(Integer id);
}
