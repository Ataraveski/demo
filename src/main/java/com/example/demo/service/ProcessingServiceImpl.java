package com.example.demo.service;

import com.example.demo.model.Card;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
public class ProcessingServiceImpl implements ProcessingService {

    private final List<Card> cardsList = new CopyOnWriteArrayList<>();

    {
        cardsList.addAll(List.of(new Card(1, 100),
                new Card(2, 1000),
                new Card(3, 2000),
                new Card(4, 5000)));
    }

    @Override
    public Flux<Card> getAllCards() {
        return Flux.fromIterable(cardsList);
    }

    @Override
    public Mono<Card> getById(Integer id) {
        return Mono.justOrEmpty(cardsList.stream().filter(el -> el.getId().equals(id)).findFirst());
    }
}
